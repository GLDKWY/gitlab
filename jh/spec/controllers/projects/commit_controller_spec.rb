# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Projects::CommitController do
  render_views
  let(:project) { create(:project, :repository, :public) }
  let(:repository) { project.repository }
  let(:user) { create(:user) }

  before do
    project.add_maintainer(user)
    sign_in(user)
  end

  describe 'GET show' do
    context 'content validation' do
      let(:commit) { repository.commit }
      let!(:content_blocked_state) { create(:content_blocked_state, container: project, commit_sha: commit.id, path: commit.raw_diffs.first.new_path) }
      let(:blocked_message) { s_("ContentValidation|According to the relevant laws and regulations, this content is not displayed.") }

      subject do
        params = {
          namespace_id: project.namespace.to_param,
          project_id: project,
          id: commit.id
        }

        get :show, params: params
      end

      before do
        allow(ContentValidation::Setting).to receive(:block_enabled?).and_return(true)
        stub_feature_flags(async_commit_diff_files: false)
      end

      it "render blocked message" do
        subject

        expect(response.body).to include(blocked_message)
      end
    end
  end
end
