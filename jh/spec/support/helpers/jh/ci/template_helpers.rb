# frozen_string_literal: true

module JH
  module Ci
    module TemplateHelpers
      extend ::Gitlab::Utils::Override

      override :secure_analyzers_prefix
      def secure_analyzers_prefix
        'gitlab-jh-public.tencentcloudcr.com/security-products/analyzers'
      end
    end
  end
end
