# frozen_string_literal: true

require 'spec_helper'

RSpec.describe SnippetBlobPresenter do
  let_it_be(:snippet) { create(:personal_snippet, :repository, :public) }

  let(:commit) { snippet.repository.commit }
  let(:blob) do
    Gitlab::Diff::FileCollection::Base.new(
      commit,
      project: snippet,
      diff_refs: commit.diff_refs
    ).diff_files.first.blob
  end

  context "content validation" do
    before do
      allow(ContentValidation::Setting).to receive(:block_enabled?).and_return(true)
      allow(Gitlab::Git::Commit).to receive(:last_for_path).and_return(commit)
    end

    let!(:content_blocked_state) { create(:content_blocked_state, container: snippet, commit_sha: commit.id, path: blob.path) }
    let(:blocked_message) { s_("ContentValidation|According to the relevant laws and regulations, this content is not displayed.") }

    context "plain_data" do
      it "return blocked message when content blocked" do
        expect(described_class.new(blob).plain_data).to include(blocked_message)
      end
    end

    context "raw_plain_data" do
      it "return blocked message when content blocked" do
        expect(described_class.new(blob).raw_plain_data).to include(blocked_message)
      end
    end

    context "rich_data" do
      it "return blocked message when content blocked" do
        expect(described_class.new(blob).rich_data).to include(blocked_message)
      end
    end
  end
end
