# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'TencentSms' do
  describe '.validate_phone_number' do
    it 'is ok if phone is China Mainland/HongKong/Macao' do
      expect { TencentSms.validate_phone_number("+8615623886888") }.not_to raise_error
      expect { TencentSms.validate_phone_number("+85292318788") }.not_to raise_error
      expect { TencentSms.validate_phone_number("+85292318799") }.not_to raise_error
    end

    it 'raises an error if phone includes illigal chars' do
      expect { TencentSms.validate_phone_number("+86-<javascript>") }.to \
        raise_error(TencentSms::PhoneNumberError, "Invalid Phone Number")
    end

    it 'raises an error if phone is unsupported region' do
      expect { TencentSms.validate_phone_number("+1100861212") }.to \
        raise_error(TencentSms::PhoneNumberError, "Unsupported Region Phone Number")
    end

    it 'raises an error if length of China Mainland phone is not 14' do
      expect { TencentSms.validate_phone_number("+86156238876") }.to \
        raise_error(TencentSms::PhoneNumberError, "Invalid China Mainland Phone Number")
    end

    it 'raises an error if length of China HongKong phone is not 11 or 12' do
      expect { TencentSms.validate_phone_number("+85212344") }.to \
        raise_error(TencentSms::PhoneNumberError, "Invalid China HongKong Phone Number")
    end

    it 'raises an error if length of China Macao phone is not 11 or 12' do
      expect { TencentSms.validate_phone_number("+85312344") }.to \
        raise_error(TencentSms::PhoneNumberError, "Invalid China Macao Phone Number")
    end
  end
end
