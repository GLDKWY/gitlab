# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Group do
  describe 'validations' do
    it_behaves_like "content validation", :group, :name
    it_behaves_like "content validation", :group, :description
  end
end
