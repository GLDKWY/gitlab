# frozen_string_literal: true

require 'spec_helper'

RSpec.describe User do
  describe 'validations' do
    it_behaves_like "content validation", :user, :name
    it_behaves_like "content validation", :user, :skype
    it_behaves_like "content validation", :user, :linkedin
    it_behaves_like "content validation", :user, :twitter
    it_behaves_like "content validation", :user, :location
    it_behaves_like "content validation", :user, :organization
  end

  describe '.user_search_minimum_char_limit' do
    it 'returns false' do
      expect(described_class.user_search_minimum_char_limit).to be(false)
    end
  end
end
