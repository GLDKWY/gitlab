# frozen_string_literal: true

module EncryptPhone
  private

  # e.g: "+8615612341234" -> "OVj5kV60tFF70495B1kq..."
  def encrypt_phone
    params[:phone] = Gitlab::CryptoHelper.aes256_gcm_encrypt(params[:phone])
  end

  def encrypt_phone_with_user
    params[:user][:phone] = Gitlab::CryptoHelper.aes256_gcm_encrypt(params[:user][:phone])
  end
end
