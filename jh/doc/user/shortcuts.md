---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
disqus_identifier: 'https://docs.gitlab.com/ee/workflow/shortcuts.html'
---

# 键盘快捷键 **(FREE)**

极狐GitLab 有一些键盘快捷键，您可以使用它们来访问其不同的功能。

要在极狐GitLab 中显示一个列出其键盘快捷键的窗口，请使用以下方法之一：

- 按 <kbd>?</kbd>.
- 在应用程序右上角的帮助菜单中，选择**键盘快捷键**。

<!--In [GitLab 12.8 and later](https://gitlab.com/gitlab-org/gitlab/-/issues/22113),-->
您可以使用键盘快捷键窗口顶部的 **键盘快捷键** 切换来禁用键盘快捷键。

尽管[全局快捷方式](#全局快捷方式)可以在任何区域工作，但您必须在特定页面中才能使用部分其它快捷方式，如以下每个部分所述。

## 全局快捷方式

以下快捷方式在大多数区域都可用：

| 键盘快捷键              | 描述 |
|---------------------------------|-------------|
| <kbd>?</kbd>                    | 显示或隐藏快捷方式参考表。 |
| <kbd>Shift</kbd> + <kbd>p</kbd> | 访问项目页面。 |
| <kbd>Shift</kbd> + <kbd>g</kbd> | 访问群组页面。 |
| <kbd>Shift</kbd> + <kbd>a</kbd> | 访问活动页面。 |
| <kbd>Shift</kbd> + <kbd>l</kbd> | 访问里程碑页面。 |
| <kbd>Shift</kbd> + <kbd>s</kbd> | 访问代码片段页面 |
| <kbd>s</kbd> / <kbd>/</kbd>     | 将光标放在搜索栏中。 |
| <kbd>Shift</kbd> + <kbd>i</kbd> | 访问议题页面。 |
| <kbd>Shift</kbd> + <kbd>m</kbd> | 访问合并请求页面。 |
| <kbd>Shift</kbd> + <kbd>t</kbd> | 访问待办事项列表页面。 |
| <kbd>p</kbd> + <kbd>b</kbd>     | 显示或隐藏性能栏。 |
| <kbd>.</kbd>     | 打开 Web IDE<!--[Web IDE](project/web_ide/index.md)-->。 |
<!--
| <kbd>g</kbd> + <kbd>x</kbd>     | 在当前版本和 [GitLab Next](https://next.gitlab.com/) (GitLab SaaS only). |
-->

此外，在文本字段（例如评论、回复、议题描述和合并请求描述）中编辑文本时，可以使用以下快捷方式：

| 键盘快捷键                                                        | 描述 |
|---------------------------------------------------------------------------|-------------|
| <kbd>↑</kbd>                                                              | 编辑您的最后一条评论。您必须位于主题下方的空白文本字段中，并且主题中必须至少有一条评论。 |
| <kbd>⌘</kbd> (Mac) / <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>p</kbd> | 在顶部具有 **编辑** 和 **预览** 选项卡的文本字段中编辑文本时，切换 Markdown 预览。 |
| <kbd>⌘</kbd> (Mac) / <kbd>Control</kbd> + <kbd>b</kbd>                    | 将所选文本加粗（用 `**` 将其括起来）。 |
| <kbd>⌘</kbd> (Mac) / <kbd>Control</kbd> + <kbd>i</kbd>                    | 将所选文本斜体（用`_` 括起来）。 |
| <kbd>⌘</kbd> (Mac) / <kbd>Control</kbd> + <kbd>k</kbd>                    | 添加链接（用`[]()` 包围所选文本）。 |

即使其他键盘快捷键被禁用，用于在文本字段中编辑的快捷键始终处于启用状态。

## 项目

这些快捷方式可从项目中的任何页面使用。您必须相对较快地键入它们才能工作，它们会将您带到项目中的另一个页面。

| 键盘快捷键           | 描述 |
|-----------------------------|-------------|
| <kbd>g</kbd> + <kbd>p</kbd> | 访问项目主页。 <!--(**Project > Details**).--> |
| <kbd>g</kbd> + <kbd>v</kbd> | 访问项目活动订阅 (**项目信息 > 动态**). |
| <kbd>g</kbd> + <kbd>f</kbd> | 访问[项目文件](#项目文件)列表 (**仓库 > 文件**)。 |
| <kbd>t</kbd>                | 访问项目文件搜索页面 (**仓库 > 文件**，选择 **查找文件**)。 |
| <kbd>g</kbd> + <kbd>c</kbd> | 访问项目提交列表 (**仓库 > 提交**)。 |
| <kbd>g</kbd> + <kbd>n</kbd> | 访问[仓库分支图](#仓库分支图)页面 (**仓库 > 分支图**)。 |
| <kbd>g</kbd> + <kbd>d</kbd> | 访问仓库图标 (**分析 > 仓库**)。 |
| <kbd>g</kbd> + <kbd>i</kbd> | 访问项目议题列表 (**议题 > 列表**)。 |
| <kbd>i</kbd>                | 访问新建议题页面 (**议题**，选择 **新建议题**)。 |
| <kbd>g</kbd> + <kbd>b</kbd> | 访问项目议题看板列表 (**议题 > 看板**)。 |
| <kbd>g</kbd> + <kbd>m</kbd> | 访问项目合并请求列表 (**合并请求**)。 |
| <kbd>g</kbd> + <kbd>j</kbd> | 访问 CI/CD 作业列表 (**CI/CD > 作业**)。 |
| <kbd>g</kbd> + <kbd>l</kbd> | 访问项目指标 (**监控 > 指标**)。 |
| <kbd>g</kbd> + <kbd>e</kbd> | 访问项目环境 (**部署 > 环境**)。 |
| <kbd>g</kbd> + <kbd>k</kbd> | 访问项目 Kubernetes 集群集成页面 (**基础设施 > Kubernetes 集群**)。请注意，您必须至少具有维护者权限<!--[维护者权限](permissions.md)-->才能访问此页面。 |
| <kbd>g</kbd> + <kbd>s</kbd> | 访问项目代码片段列表 (**代码片段**)。 |
| <kbd>g</kbd> + <kbd>w</kbd> | 访问项目 wiki (**Wiki**)，如果已启用。 |

<!--
| <kbd>g</kbd> + <kbd>r</kbd> | 访问项目发布列表 (**Project > Releases**). |
-->

### 议题和合并请求

查看议题和合并请求时可以使用以下快捷方式：

| 键盘快捷键             | 描述 |
|------------------------------|-------------|
| <kbd>e</kbd>                 | 编辑描述。 |
| <kbd>a</kbd>                 | 更改指派人。 |
| <kbd>m</kbd>                 | 更改里程碑。 |
| <kbd>l</kbd>                 | 更改标记。 |
| <kbd>r</kbd>                 | 开始写评论。评论中引用了预先选择的文本。不能用于在主题中回复。 |
| <kbd>n</kbd>                 | 转到下一个未解决的讨论（仅限合并请求）。 |
| <kbd>p</kbd>                 | 转到上一个未解决的讨论（仅限合并请求）。 |
| <kbd>]</kbd> or <kbd>j</kbd> | 移至下一个文件（仅限合并请求）。 |
| <kbd>[</kbd> or <kbd>k</kbd> | 移至上一个文件（仅限合并请求）。 |
| <kbd>b</kbd>                 | 复制源分支名称（仅限合并请求）。 |
| <kbd>.</kbd>     | 打开 Web IDE<!--[Web IDE](project/web_ide/index.md)-->。 |

### 项目文件

浏览项目中的文件时可以使用这些快捷方式 (赚到 **仓库 > 文件**)：

| 键盘快捷键  | 描述 |
|-------------------|-------------|
| <kbd>↑</kbd>      | 上移选择。 |
| <kbd>↓</kbd>      | 下移选择。 |
| <kbd>enter</kbd>  | 打开所选。 |
| <kbd>Escape</kbd> | 返回文件列表屏幕 (仅在搜索文件时，**仓库 > 文件**，然后选择 **查找文件**)。 |
| <kbd>y</kbd>      | 转到文件永久链接（仅在查看文件时）。 |
| <kbd>.</kbd>     | 打开 Web IDE<!--[Web IDE](project/web_ide/index.md)-->。 |

### Web IDE

使用 Web IDE<!--[Web IDE](project/web_ide/index.md)--> 编辑文件时，可以使用以下快捷方式：

| 键盘快捷键                                           | 描述 |
|------------------------------------------------------------|-------------|
| <kbd>⌘</kbd> (Mac) / <kbd>Control</kbd> + <kbd>p</kbd>     | 搜索，然后打开另一个文件进行编辑。 |
| <kbd>⌘</kbd> (Mac) / <kbd>Control</kbd> + <kbd>Enter</kbd> | 提交（在编辑提交消息时）。 |

### 仓库分支图

查看仓库分支图<!--[仓库分支图](project/repository/index.md#repository-history-graph)-->页面时（导航到 **仓库 > 分支图**），可以使用以下快捷方式：

| 键盘快捷键                                                   | 描述 |
|--------------------------------------------------------------------|-------------|
| <kbd>←</kbd> or <kbd>h</kbd>                                       | 向左滚动。 |
| <kbd>→</kbd> or <kbd>l</kbd>                                       | 向右滚动。 |
| <kbd>↑</kbd> or <kbd>k</kbd>                                       | 向上滚动。 |
| <kbd>↓</kbd> or <kbd>j</kbd>                                       | 向下滚动。 |
| <kbd>Shift</kbd> + <kbd>↑</kbd> or <kbd>Shift</kbd> + <kbd>k</kbd> | 滚动到顶部。 |
| <kbd>Shift</kbd> + <kbd>↓</kbd> or <kbd>Shift</kbd> + <kbd>j</kbd> | 滚动到底部。 |

### Wiki 页面

查看 wiki 页面<!--[wiki 页面](project/wiki/index.md)-->时，可以使用以下快捷方式：

| 键盘快捷键  | 描述 |
|-------------------|-------------|
| <kbd>e</kbd>      | 编辑 wiki 页面。 |

### 过滤搜索

使用过滤搜索输入<!--[过滤搜索输入](search/index.md)-->时，可以使用以下快捷方式：

| 键盘快捷键                                       | 描述 |
|--------------------------------------------------------|-------------|
| <kbd>⌘</kbd> (Mac) + <kbd>⌫</kbd>                      | 清除整个搜索过滤器。 |
| <kbd>⌥</kbd> (Mac) / <kbd>Control</kbd> + <kbd>⌫</kbd> | 一次清除一个令牌。 |

## 史诗 **(ULTIMATE)**

查看[史诗](group/epics/index.md)时可以使用以下快捷方式：

| 键盘快捷键  | 描述   |
|-------------------|-------------|
| <kbd>r</kbd>      | 开始写评论。评论中引用了预先选择的文本。不能用于在主题中回复。 |
| <kbd>e</kbd>      | 编辑描述。 |
| <kbd>l</kbd>      | 更改标记。 |
