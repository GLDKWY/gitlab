---
type: concepts, howto
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 个人访问令牌 **(FREE)**

> - 引入于 12.6 版本：令牌到期通知。
> - 引入于旗舰版 12.6 版本：令牌生命周期限制。
> - 引入于 13.3 版本：令牌过期的附加通知。
> - 引入于 14.1 版本：预填充令牌名称和范围。

个人访问令牌可以替代 OAuth2<!--[OAuth2](../../api/oauth2.md)--> 并用于：

- 使用 GitLab API<!--[GitLab API](../../api/index.md#personalproject-access-tokens)--> 进行身份验证。
- 使用 HTTP 基本身份验证与 Git 进行身份验证。

在这两种情况下，您都使用个人访问令牌代替密码进行身份验证。

个人访问令牌是：

- 启用双重身份验证（2FA）<!--[双重身份验证 (2FA)](account/two_factor_authentication.md)-->时需要。
- 与极狐GitLab 用户名一起使用，需要用户名的功能可以进行身份验证。例如，极狐GitLab 管理的 Terraform 状态后端<!--[GitLab managed Terraform state backend](../infrastructure/iac/terraform_state.md#using-a-gitlab-managed-terraform-state-backend-as-a-remote-data-source)-->和 Docker container registry<!--[Docker container registry](../packages/container_registry/index.md#authenticate-with-the-container-registry)-->，
- 类似于项目访问令牌<!--[项目访问令牌](../project/settings/project_access_tokens.md)-->，但附加到用户而不是项目。

NOTE:
尽管是必需的，但在使用个人访问令牌进行身份验证时，会忽略极狐GitLab 用户名。
<!--There is an [issue for tracking](https://gitlab.com/gitlab-org/gitlab/-/issues/212953) to make GitLab
use the username.-->

<!--有关如何使用个人访问令牌对 API 进行身份验证的示例，请参阅 [API 文档](../../api/index.md#personalproject-access-tokens)。

或者，GitLab 管理员可以使用 API 创建 [模拟令牌](../../api/index.md#impersonation-tokens)。 使用模拟令牌以特定用户身份自动进行身份验证。-->

## 创建个人访问令牌

您可以根据需要创建任意数量的个人访问令牌。

1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **访问令牌**。
1. 输入令牌的名称和可选的到期日期。
1. 设置[选择范围](#个人访问令牌范围)。
1. 选择 **创建个人访问令牌**。

将个人访问令牌保存在安全的地方。离开页面后，您将无法再访问令牌。

### 预填充个人访问令牌名称和范围

您可以直接链接到个人访问令牌页面，并在表单中预先填写名称和范围列表。为此，您可以向 URL 附加一个 `name` 参数和以逗号分隔的范围列表。例如：

```plaintext
https://gitlab.example.com/-/profile/personal_access_tokens?name=Example+Access+token&scopes=api,read_user,read_registry
```

WARNING:
必须谨慎对待个人访问令牌。<!--Read our [token security considerations](../../security/token_overview.md#security-considerations)
for guidance on managing personal access tokens (for example, setting a short expiry and using minimal scopes).-->

## 撤销个人访问令牌

您可以随时撤销个人访问令牌。

1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **访问令牌**。
1. 在 **有效的个人访问令牌** 区域，在密钥旁边，选择 **撤销**。

## 查看上次使用令牌的时间

令牌使用情况每 24 小时更新一次。每次使用令牌请求 API 资源<!--[API 资源](../../api/api_resources.md)-->和 <!--[GraphQL API](../../api/graphql/index.md)-->GraphQL API 时都会更新它。

查看上次使用令牌的时间：

1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **访问令牌**。
1. 在 **有效的个人访问令牌** 区域，在密钥旁边，查看 **最近使用** 日期。

## 个人访问令牌范围

个人访问令牌可以根据分配的范围执行操作。

| 范围              | 访问 |
|--------------------|--------|
| `api`              | 完整 API 读写，包括所有群组和项目、容器镜像库和软件包库。 |
| `read_user`        | `/users` 下的端点只读。本质上，访问 <!--[Users API](../../api/users.md)--> 中的任何 `GET` 请求。 |
| `read_api`         | 完整 API 只读，包括所有群组和项目、容器镜像库和软件包库。（引入于 12.10 版本） |
| `read_repository`  | 通过 `git clone` 对仓库进行只读（拉取）。 |
| `write_repository` | 通过 `git clone` 对仓库进行读写（拉取、推送）。启用 2FA 时通过 HTTP 访问 Git 仓库是必需的。 |
| `read_registry`    | 如果项目是私有的并且需要授权，则 Container Registry<!--[Container Registry](../packages/container_registry/index.md)--> 镜像只读（拉取）。 |
| `write_registry`   | 如果项目是私有的并且需要授权，则对 Container Registry<!--[Container Registry](../packages/container_registry/index.md)--> 镜像进行读写（推送）。（引入于 12.10 版本） |
| `sudo`             | 作为系统中任何用户的 API 操作（如果经过身份验证的用户是管理员）。 |

## 个人访问令牌到期时

个人访问令牌在您定义的日期（UTC 午夜）到期。

- 极狐GitLab 在每天 01:00 AM UTC 运行检查，以识别在接下来的 7 天内到期的个人访问令牌。这些令牌的所有者会收到电子邮件通知。
- 极狐GitLab 每天在 UTC 时间凌晨 02:00 运行检查，以识别在当前日期过期的个人访问令牌。这些令牌的所有者会收到电子邮件通知。
- 在旗舰版中，管理员可以限制个人访问令牌的生命周期<!--[限制个人访问令牌的生命周期](../admin_area/settings/account_and_limit_settings.md#limit-the-lifetime-of-personal-access-tokens)-->。
- 在旗舰版中，管理员可以选择是否强制个人访问令牌过期<!--[强制个人访问令牌过期](../admin_area/settings/account_and_limit_settings.md#allow-expired-personal-access-tokens-to-be-used)-->。

## 以编程方式创建个人访问令牌 **(FREE SELF)**

您可以创建预先确定的个人访问令牌作为测试或自动化的一部分。

先决条件：

- 您需要足够的访问权限才能为您的实例运行 Rails 控制台会话<!--[Rails 控制台会话](../../administration/operations/rails_console.md#starting-a-rails-console-session)-->。

以编程方式创建个人访问令牌：

1. 打开 Rails 控制台：

   ```shell
   sudo gitlab-rails console
   ```

1. 运行以下命令以引用用户名、令牌和范围。

    令牌长度必须为 20 个字符。范围必须有效并且[在源代码中](https://gitlab.com/gitlab-jh/gitlab/-/blob/master/lib/gitlab/auth.rb)可见。

    例如，要创建属于用户名为 `automation-bot` 的用户的令牌：

   ```ruby
   user = User.find_by_username('automation-bot')
   token = user.personal_access_tokens.create(scopes: [:read_user, :read_repository], name: 'Automation token')
   token.set_token('token-string-here123')
   token.save!
   ```

可以使用 Rails runner<!--[Rails runner](../../administration/troubleshooting/debug.md#using-the-rails-runner)--> 将此代码缩短为单行 shell 命令：

```shell
sudo gitlab-rails runner "token = User.find_by_username('automation-bot').personal_access_tokens.create(scopes: [:read_user, :read_repository], name: 'Automation token'); token.set_token('token-string-here123'); token.save!"
```

## 以编程方式撤销个人访问令牌 **(FREE SELF)**

作为测试或自动化的一部分，您可以以编程方式撤销个人访问令牌。

先决条件：

- 您需要足够的访问权限才能为您的实例运行 Rails 控制台会话<!--[Rails 控制台会话](../../administration/operations/rails_console.md#starting-a-rails-console-session)-->。

以编程方式撤销令牌：

1. 打开 Rails 控制台：

   ```shell
   sudo gitlab-rails console
   ```

1. 要撤销 `token-string-here123`的令牌，请运行以下命令：

   ```ruby
   token = PersonalAccessToken.find_by_token('token-string-here123')
   token.revoke!
   ```

可以使用 Rails runner<!--[Rails runner](../../administration/troubleshooting/debug.md#using-the-rails-runner)--> 将此代码缩短为单行 shell 命令：

```shell
sudo gitlab-rails runner "PersonalAccessToken.find_by_token('token-string-here123').revoke!"
```

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
