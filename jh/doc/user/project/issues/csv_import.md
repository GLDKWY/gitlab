---
stage: Manage
group: Import
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 从 CSV 导入议题 **(FREE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/23532) in GitLab 11.7.
-->

可以通过上传包含 `title` 和 `description` 列的 CSV 文件将议题导入到项目中。其他列**未**导入。如果要保留标签和里程碑等列，请考虑使用[移动议题功能](managing_issues.md#移动议题)。

上传 CSV 文件的用户被设置为导入议题的作者。

NOTE:
导入议题需要开发者<!--[开发者](../../permissions.md)-->或更高的权限级别。

## 准备导入

- 考虑导入仅包含几个议题的测试文件。如果不使用 API，则无法撤消大型导入。
- 确保您的 CSV 文件符合[文件格式](#csv-文件格式)要求。

## 导入文件

要导入议题：

1. 导航到项目的议题列表页面。
1. 如果存在现有议题，请单击右上角 **编辑问题** 按钮旁边的导入图标。
1. 对于没有任何议题的项目，单击页面中间标有 **导入 CSV** 的按钮。
1. 选择文件并单击 **导入议题** 按钮。

该文件在后台进行处理，并在导入完成后向您发送通知电子邮件。

## CSV 文件格式

从 CSV 文件导入议题时，必须以某种方式对其进行格式化：

- **标题行：** CSV 文件必须包含以下标题：`title` 和 `description`。标题的大小写无关紧要。
- **列：** 不导入 `title` 和`description` 之外的列中的数据。
- **分隔符：** 从标题行中自动检测列分隔符。支持的分隔符有：逗号（`,`）、分号（`;`）和制表符（`\t`）。行分隔符可以是 `CRLF` 或 `LF`。
- **双引号字符：** 双引号 (`"`) 字符用于引用字段，允许在字段中使用列分隔符（请参阅下面示例 CSV 数据中的第三行）。插入双引号 (`"`) 在带引号的字段中，连续使用两个双引号字符，即 `""`。
- **数据行：** 在标题行之后，后续行必须遵循相同的列顺序。议题标题是必需的，而描述是可选的。

如果字段中有特殊字符（例如 `\n` 或 `,`），请将这些字符用双引号括起来。

示例 CSV 数据：

```plaintext
title,description
My Issue Title,My Issue Description
Another Title,"A description, with a comma"
"One More Title","One More Description"
```

### 文件大小

该限制取决于实例的 Max Attachment Size 的配置值。

<!--
对于 GitLab.com，它设置为 10 MB。-->
