---
stage: Create
group: Editor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: howto
---

# GitLab Web 编辑器 **(FREE)**

有时，直接从 GitLab 界面进行快速更改比克隆项目并使用 Git 命令行工具更容易。在此功能亮点中，我们将了解如何从文件浏览器创建新文件、目录、分支或标签。所有这些操作都可以从一个下拉菜单中获得。

## 创建文件

在项目的文件页面中，单击分支选择器右侧的 `+` 按钮。
从下拉列表中选择 **新建文件**。
![New file dropdown menu](img/web_editor_new_file_dropdown_v14_1.png)

在**文件名** 框中输入文件名。然后，在编辑器区域添加文件内容。添加描述性提交消息并选择一个分支。分支字段默认为您在文件浏览器中查看的分支。 如果您输入新的分支名称，则会显示一个复选框，允许您在提交更改后启动新的合并请求。

如果您对新文件感到满意，请单击底部的 **提交更改**。

![Create file editor](img/web_editor_new_file_editor_v14_1.png)

### 快捷方式

您可以在通过 Web 编辑器编辑文件时使用快捷方式。它使用与 Web IDE 相同的快捷方式。有关详细信息，请阅读[命令面板](../web_ide/index.md#命令面板) 的文档。

### 模板下拉菜单

启动新项目时，新项目可能需要一些通用文件。极狐GitLab 会显示一条消息来帮助您：

![First file for your project](img/web_editor_template_dropdown_first_file_v14_1.png)

当单击 `LICENSE` 或 `.gitignore` 等时，会显示一个下拉列表，为您提供可能适合您项目的模板：

![MIT license selected](img/web_editor_template_dropdown_mit_license_v14_1.png)

许可证、变更日志、贡献指南或 `.gitlab-ci.yml` 文件也可以通过项目页面上的按钮添加。在此示例中，已创建许可证，这会创建到许可证本身的链接。

![New file button](img/web_editor_template_dropdown_buttons.png)

NOTE:
**设置 CI/CD** 按钮不会出现在空仓库中。要显示按钮，请将文件添加到您的仓库。

## 高亮行

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/56159) in GitLab 13.10 for GitLab SaaS instances.
-->

> - 适用自助管理版于 13.11 版本。

Web 编辑器使您能够通过向 URL 的文件路径段添加特殊格式的哈希信息来突出显示一行。例如，文件路径段 `MY_FILE.js#L3` 指示 Web 编辑器突出显示第 3 行。

Web 编辑器还使您能够使用类似的模式突出显示多行。在这种情况下，文件路径段 `MY_FILE.js#L3-10` 指示 Web 编辑器突出显示文件的第 3 到 10 行。

您不需要手动构建这些行。相反，您可以：

1. 将鼠标悬停在共享时要突出显示的行号上。
1. 用鼠标右键单击数字。
1. 单击上下文菜单中的 **复制链接地址**。

   ![Link to a line](img/web_editor_line_link_v13_10.png)

## 上传文件

当内容是文本时，创建文件的能力非常好。但是，这不适用于二进制数据，例如图像、PDF 或其他二进制文件类型。在这种情况下，您需要上传文件。

在项目的文件页面中，单击分支选择器右侧的 `+` 按钮。从下拉菜单中选择 **上传文件**：

![Upload file dropdown menu](img/web_editor_upload_file_dropdown_v14_1.png)

弹出上传对话框后，有两种上传文件的方法。在弹出窗口中拖放文件或使用 **点击上传** 链接。选择要上传的文件后，将显示文件预览。

输入提交信息，选择一个分支，准备好后点击 **上传文件**。

![Upload file dialog](img/web_editor_upload_file_dialog_v14_1.png)

## 创建目录

为了使仓库中的文件井井有条，创建一个新目录通常很有帮助。

在项目的文件页面中，单击分支选择器右侧的加号按钮 (`+`)。
从下拉列表中选择 **新建目录**。

![New directory dropdown](img/web_editor_new_directory_dropdown_v14_1.png)

在新目录对话框中，输入目录名称、提交消息，然后选择目标分支。点击 **创建目录** 完成。

![New directory dialog](img/web_editor_new_directory_dialog_v14_1.png)

## 创建新分支

有多种方法可以从 GitLab Web 界面创建分支。

### 从一个议题创建一个新分支

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/2808) in GitLab 8.6.
-->

如果您的开发工作流程需要为每个合并请求创建一个议题，您可以直接从该议题创建一个分支以加快流程。
新分支以及后来的合并请求被标记为与此议题相关。
合并后，合并请求将关闭议题。
您可以在议题描述下方看到 **创建合并请求** 下拉列表。

如果出现以下情况，**创建合并请求** 按钮不会显示：

- 已存在同名分支。
- 此分支已存在合并请求。
- 您的项目有一个有效的派生关系。

要显示此按钮，一种可能的解决方法是删除项目的派生关系<!--[删除项目的分叉关系](../settings/index.md#removing-a-fork-relationship)-->。
移除后，派生关系无法恢复。此项目无法再向源项目或其他分支接收或发送合并请求。

![Create Button](img/web_editor_new_branch_from_issue_create_button_v14_1.png)

此下拉列表包含选项 **创建合并请求和分支** 和 **创建分支**。

![New Branch Button](img/web_editor_new_branch_from_issue_v14_1.png)

选择这些选项之一后，将根据项目的[默认分支](branches/default.md)创建新分支或分支合并请求。
分支名称基于内部 ID 和议题标题。上面的示例屏幕截图创建了一个名为 `2-make-static-site-auto-deploy-and-serve` 的分支。

当您单击空存储库项目中的 **创建分支** 按钮时，极狐GitLab 会执行以下操作：

- 创建一个默认分支。
- 向其提交一个空白的 `README.md` 文件。
- 根据议题标题创建并将您重定向到新分支。
- *如果您的项目像 Kubernetes 一样配置了部署服务<!--[配置了部署服务](../integrations/overview.md)-->，* 系统会提示您设置自动部署<!--[自动部署](../../../topics/autodevops/stages.md#auto-deploy)-->，帮助您创建一个 `.gitlab-ci.yml` 文件。

创建分支后，您可以编辑仓库中的文件以解决议题。当基于新创建的分支创建合并请求时，描述字段显示[议题关闭模式](../issues/managing_issues.md#自动关闭议题) `Closes #ID`，其中 `ID` 是议题的 ID。当合并请求被合并时，将关闭议题。

### 从项目的仪表板创建一个新分支

如果您想在创建新的合并请求之前对多个文件进行更改，您可以预先创建一个新的分支。

1. 从项目的文件页面，从下拉列表中选择**新建分支**。

   ![New branch dropdown](img/web_editor_new_branch_dropdown_v14_1.png)

1. 输入一个新的**分支名称**。
1. （可选）更改 **创建自** 字段以选择此新分支源自哪个分支、标记或提交 SHA。如果您开始输入现有分支或标签，此字段会自动完成。
1. 点击 **创建分支** 返回到这个新分支上的文件浏览器。

   ![New branch page](img/web_editor_new_branch_page_v14_1.png)

您现在可以根据需要更改任何文件。当您准备好将更改合并回[默认分支](branches/default.md)时，您可以使用屏幕顶部的部件。
此部件仅在您创建分支或修改文件后出现一段时间。

![New push widget](img/web_editor_new_push_widget.png)

## 创建新标签

标签可帮助您标记主要里程碑，例如生产版本和候选版本。您可以从分支或提交 SHA 创建标签：

1. 从项目的文件页面，从下拉列表中选择 **新建标签**。

   ![New tag dropdown](img/web_editor_new_tag_dropdown.png)

1. 给标签起一个名字，比如 `v1.0.0`。
1. 选择要从中创建此新标签的分支或 SHA。
1. （可选）添加消息和版本说明。发行说明部分支持 Markdown 格式。
1. （可选）上传附件。
1. 点击 **创建标签**，系统会跳转到标签列表页面。

   ![New tag page](img/web_editor_new_tag_page.png)

## Tips

创建或上传新文件或创建新目录时，您可以触发新的合并请求，而不是直接提交到默认分支：

1. 在 **目标分支** 字段中输入新的分支名称。
1. 系统显示 **使用这些更改开始新的合并请求** 复选框。
1. 提交您的更改，系统会将您重定向到新的合并请求表单。

   ![Start a new merge request with these changes](img/web_editor_start_new_merge_request.png)

如果您不想将您的主要电子邮件地址用于通过网络编辑器创建的提交，您可以从 **用户设置 > 编辑个人资料** 页面选择使用另一个链接的电子邮件地址。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
