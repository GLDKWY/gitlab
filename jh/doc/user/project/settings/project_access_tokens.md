---
stage: Manage
group: Access
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, howto
---

# 项目访问令牌

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/210181) in GitLab 13.0.
> - [Became available on GitLab.com](https://gitlab.com/gitlab-org/gitlab/-/issues/235765) in GitLab 13.5 for paid groups only.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/235765) in GitLab 13.5.
-->

您可以使用项目访问令牌进行身份验证：

- 使用 GitLab API<!--[GitLab API](../../../api/index.md#personalproject-access-tokens)-->。
- 使用 Git，当使用 HTTP 基本身份验证时。

配置项目访问令牌后，进行身份验证时不需要密码。相反，您可以输入任何非空白值。

项目访问令牌类似于[个人访问令牌](../../profile/personal_access_tokens.md)，不同之处在于它们与项目而不是用户相关联。

您可以使用项目访问令牌：


- 在 SaaS 版上，如果您拥有专业版或更高级别许可证。个人访问令牌不适用于[试用许可证](https://about.gitlab.cn/free-trial/)。

- 在具有任何许可证级别的自助管理实例上。如果您有标准版：
  - 查看围绕用户自行注册<!--[用户自行注册](../../admin_area/settings/sign_up_restrictions.md#disable-new-sign-ups)-->的安全和合规性政策。
  - 考虑[禁用项目访问令牌](#启用或禁用项目访问令牌创建)以减少潜在的滥用。

项目访问令牌继承为个人访问令牌配置的默认前缀设置<!--[默认前缀设置](../../admin_area/settings/account_and_limit_settings.md#personal-access-token-prefix)-->。

## 创建项目访问令牌

要创建项目访问令牌：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 访问令牌**。
1. 输入名称。
1. 可选。输入令牌的到期日期。 令牌将在该日期的 UTC 午夜到期。
1. 为令牌选择一个角色。
1. 选择[所需范围](#项目访问令牌的范围)。
1. 选择 **创建项目访问令牌**。

显示项目访问令牌。 将项目访问令牌保存在安全的地方。 离开或刷新页面后，将无法再次查看。

## 撤销项目访问令牌

要撤销项目访问令牌：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 访问令牌**。
1. 在要撤销的项目访问令牌旁边，选择 **撤销**。

## 项目访问令牌的范围

范围决定了您在使用项目访问令牌进行身份验证时可以执行的操作。

| 范围              | 描述                                                                                                                                                 |
|:-------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `api`              | 授予对范围项目 API 的完整读写访问权限，包括 [Package Registry](../../packages/package_registry/index.md)。               |
| `read_api`         | 授予对范围项目 API 的读取访问权限，包括 [Package Registry](../../packages/package_registry/index.md)。                                   |
| `read_registry`    | 如果项目是私有的并且需要授权，则允许对 [Container Registry](../../packages/container_registry/index.md) 镜像进行读取访问（拉取）。 |
| `write_registry`   | 允许对 [Container Registry](../../packages/container_registry/index.md) 进行写访问（推送）。                                                             |
| `read_repository`  | 允许对仓库进行读取访问（拉取）。                                                                                                                |
| `write_repository` | 允许对仓库进行读写访问（拉取和推送）。                                                                                            |

## 启用或禁用项目访问令牌创建

> 引入于 13.11 版本。

要为顶级组中的所有项目启用或禁用项目访问令牌创建：

1. 在顶部栏上，选择 **菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 展开 **权限、LFS、2FA**。
1. 在 **权限** 下，开启或关闭 **允许创建项目访问令牌**。

即使创建被禁用，您仍然可以使用和撤销现有的项目访问令牌。

## 群组访问令牌 **(FREE SELF)**

使用群组访问令牌，您可以使用单个令牌来：

- 为群组执行操作。
- 管理群组内的项目。
- 在 14.2 及更高版本中，通过 HTTPS 使用 Git 进行身份验证。

NOTE:
您不能使用 UI 创建群组访问令牌。本节介绍了一种解决方法。<!--[An issue exists](https://gitlab.com/gitlab-org/gitlab/-/issues/214045)
to add this functionality.-->

如果您是自助管理实例的管理员，您可以在 [Rails 控制台](../../../administration/operations/rails_console.md) 中创建群组访问令牌。

<!--
<div class="video-fallback">
  For a demo of the group access token workaround, see <a href="https://www.youtube.com/watch?v=W2fg1P1xmU0">Demo: Group Level Access Tokens</a>.
</div>
<figure class="video-container">
  <iframe src="https://www.youtube.com/embed/W2fg1P1xmU0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
-->

### 创建群组访问令牌

要创建群组访问令牌：

1. 在 Rails 控制台<!--[Rails 控制台](../../../administration/operations/rails_console.md)-->中运行以下命令：

   ```ruby
   # Set the GitLab administration user to use. If user ID 1 is not available or is not an adinistrator, use 'admin = User.admins.first' instead to select an admininistrator.
   admin = User.find(1)

   # Set the group group you want to create a token for. For example, group with ID 109.
   group = Group.find(109)

   # Create the group bot user. For further group access tokens, the username should be group_#{group.id}_bot#{bot_count}. For example, group_109_bot2 and email address group_109_bot2@example.com.
   bot = Users::CreateService.new(admin, { name: 'group_token', username: "group_#{group.id}_bot", email: "group_#{group.id}_bot@example.com", user_type: :project_bot }).execute
   
   # Confirm the group bot.
   bot.confirm

   # Add the bot to the group with the required role.
   group.add_user(bot, :maintainer)

   # Give the bot a personal access token.
   token = bot.personal_access_tokens.create(scopes:[:api, :write_repository], name: 'group_token')

   # Get the token value.
   gtoken = token.token
   ```

1. 测试生成的群组访问令牌是否有效：

   1. 将 `PRIVATE-TOKEN` header 中的群组访问令牌与 GitLab REST API 一起使用。例如：
  
      - 在群组中创建史诗。
      - 在群组中的项目之一创建项目流水线。
      - 在群组中的项目之一创建议题。
  
   1. 使用群组令牌通过 HTTPS [克隆群组的项目](../../../gitlab-basics/start-using-git.md#用-ssh-进行克隆)。

### 撤销群组访问令牌

要撤销组访问令牌，请在 Rails 控制台<!--[Rails 控制台](../../../administration/operations/rails_console.md)-->中运行以下命令：

```ruby
bot = User.find_by(username: 'group_109_bot') # the owner of the token you want to revoke
token = bot.personal_access_tokens.last # the token you want to revoke
token.revoke!
```

## 项目机器人用户

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/210181) in GitLab 13.0.
> - [Excluded from license seat use](https://gitlab.com/gitlab-org/gitlab/-/issues/223695) in GitLab 13.5.
-->

项目机器人用户是系统创建的服务帐户。
每次创建项目访问令牌时，都会创建一个机器人用户并将其添加到项目中。
这些机器人用户不算作许可席位。

机器人用户具有与项目访问令牌所选角色和[范围](#项目访问令牌的范围)对应的权限。

- 名称设置为令牌的名称。
- 第一个访问令牌的用户名设置为 `project_{project_id}_bot`。例如，`project_123_bot`。
- 电子邮件设置为 `project{project_id}_bot@example.com`。例如，`project123_bot@example.com`。
- 对于同一项目中的其他访问令牌，用户名设置为 `project_{project_id}_bot{bot_count}`。例如，`project_123_bot1`。
- 对于同一项目中的其他访问令牌，电子邮件设置为 `project{project_id}_bot{bot_count}@example.com`。例如，`project123_bot1@example.com`。

使用项目访问令牌进行的 API 调用与相应的机器人用户相关联。

机器人用户：

- 包含在项目的成员列表中但不能修改。
- 不能添加到任何其他项目。

当项目访问令牌被[撤销](#撤销项目访问令牌)时：

- 机器人用户被删除。
- 所有记录都移动到用户名为 `Ghost User` 的系统范围内的用户。有关详细信息，请参阅[关联记录](../../profile/account/delete_account.md#关联记录)。