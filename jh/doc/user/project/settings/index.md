---
stage: Manage
group: Workspace
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, index, howto
---

# 项目设置 **(FREE)**

**设置** 页面为您的[项目](../index.md)配置选项提供了一个集中的主页。要访问它，请转到您项目的主页，然后在左侧导航菜单中单击 **设置**。为降低复杂性，设置按话题分组为多个部分。要显示某个部分中的所有设置，请单击 **展开**。

<!--
In GitLab versions [13.10 and later](https://gitlab.com/groups/gitlab-org/-/epics/4842),
GitLab displays a search box to help you find the settings you want to view.
-->

NOTE:
只有拥有项目维护者角色的用户和管理员才能访问项目设置。

## 通用设置

在项目的通用设置下，您可以找到与项目功能相关的所有内容。

### 通用项目设置

调整项目名称、描述、头像、<!--[默认分支](../repository/branches/default.md)-->默认分支和主题：

![general project settings](img/general_settings_v13_11.png)

项目描述也部分支持标准 Markdown<!--[标准 Markdown](../../markdown.md#features-extended-from-standard-markdown)-->。<!--您可以使用 [emphasis](../../markdown.md#emphasis)、[links](../../markdown.md#links) 和 [line-breaks](../../markdown.md#line-breaks) 为项目描述添加更多上下文。-->

#### 合规框架 **(PREMIUM)**

> - 引入于 13.9 版本
> - 功能标志移除于 13.12 版本

您可以创建合规框架标记来标识您的项目具有某些合规要求或需要额外监督。标记可以选择应用[合规流水线配置](#合规流水线配置)。

群组所有者可以创建、编辑和删除合规性框架：

1. 在顶部栏上，选择 **菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **设置** > **通用**。
1. 展开 **合规性框架** 部分。

然后可以使用以下方法将创建的合规性框架分配给群组内的项目：

- GitLab UI，使用项目设置页面
- 在 14.2 及更高版本，使用 GraphQL API<!--[GraphQL API](../../../api/graphql/reference/index.md#mutationprojectsetcomplianceframework)-->。

NOTE:
如果用户具有正确的权限，则使用 GraphQL 在子组上创建合规性框架会导致在根祖先上创建框架。UI 提供了一个只读视图来阻止这种行为。

#### 合规流水线配置 **(ULTIMATE)**

> - 引入于 13.9 版本。通过 `ff_evaluate_group_level_compliance_pipeline` 功能标志默认禁用。
> - 默认启用于 13.11 版本。
> - 功能标志移除于 14.2 版本。

群组所有者可以使用合规性流水线配置向项目添加额外的流水线配置，以定义合规性要求，例如扫描或测试。

[合规框架](#合规框架)允许群组所有者指定在专用项目中存储和管理的合规流水线配置的位置，与常规项目分开。

设置合规性框架时，使用 **合规性流水线配置** 框，将合规性框架链接到特定的 CI/CD 配置。使用 `path/file.y[a]ml@group-name/project-name` 格式。 例如：

- `.compliance-ci.yml@gitlab-org/gitlab`.
- `.compliance-ci.yaml@gitlab-org/gitlab`.

此配置由应用了合规性框架标志的项目继承。结果除了项目自己的 CI/CD 配置外，还会强制带有标记的项目运行合规性 CI/CD 配置。当带有合规性框架标签的项目执行流水线时，它会按以下顺序评估配置：

1. 合规流水线配置。
1. 项目特定的流水线配置。

在项目中运行流水线的用户必须至少具有合规性项目的报告者角色。

示例 `.compliance-gitlab-ci.yml`：

```yaml
# Allows compliance team to control the ordering and interweaving of stages/jobs.
# Stages without jobs defined will remain hidden.
stages:
  - pre-compliance
  - build
  - test
  - pre-deploy-compliance
  - deploy
  - post-compliance

variables:  # Can be overridden by setting a job-specific variable in project's local .gitlab-ci.yml
  FOO: sast

sast:  # None of these attributes can be overridden by a project's local .gitlab-ci.yml
  variables:
    FOO: sast
  image: ruby:2.6
  stage: pre-compliance
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"
      when: never
    - when: always  # or when: on_success
  allow_failure: false
  before_script:
    - "# No before scripts."
  script:
    - echo "running $FOO"
  after_script:
    - "# No after scripts."

sanity check:
  image: ruby:2.6
  stage: pre-deploy-compliance
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"
      when: never
    - when: always  # or when: on_success
  allow_failure: false
  before_script:
    - "# No before scripts."
  script:
    - echo "running $FOO"
  after_script:
    - "# No after scripts."

audit trail:
  image: ruby:2.6
  stage: post-compliance
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"
      when: never
    - when: always  # or when: on_success
  allow_failure: false
  before_script:
    - "# No before scripts."
  script:
    - echo "running $FOO"
  after_script:
    - "# No after scripts."

include:  # Execute individual project's configuration (if project contains .gitlab-ci.yml)
  project: '$CI_PROJECT_PATH'
  file: '$CI_CONFIG_PATH'
  ref: '$CI_COMMIT_REF_NAME'  # Must be defined or MR pipelines always use the use default branch.
```

##### 确保合规性作业始终运行

合规性流水线使用 GitLab CI/CD 为您提供难以置信的灵活性，用于定义您喜欢的任何类型的合规性工作。根据您的目标，这些作业可以配置为：

- 由用户修改。
- 不可修改。

在高级别上，如果合规性工作中的值：

- 已设置，不能被项目级配置更改或覆盖。
- 未设置，可以设置项目级配置。

根据您的用例，可能需要或不需要。

有一些最佳实践可以确保这些作业始终按照您定义的方式运行，并且下游的项目级流水线配置无法更改它们：

- 为您的每个合规性工作添加一个 `rules:when:always` 块。这确保它们不可修改并且始终运行。
- 显式设置作业引用的任何变量：
  - 确保项目级流水线配置不会设置它们并改变它们的行为。
  - 包括推动作业逻辑的任何作业。
- 显式设置容器镜像文件以在其中运行作业。可确保您的脚本步骤在正确的环境中执行。
- 显式设置任何相关的预定义作业关键字<!--[作业关键字](../../../ci/yaml/index.md#job-keywords)-->。可确保您的作业使用您想要的设置，并且它们不会被项目级流水线覆盖。

##### 避免父流水线和自流水线

合规性流水线始于相关项目中*每个*流水线的运行。这意味着如果相关项目中的流水线触发子流水线，则合规流水线首先运行。可以触发父流水线，而不是子流水线。

因此，在具有合规性框架的项目中，我们建议将父子流水线<!--[父子流水线](../../../ci/pipelines/parent_child_pipelines.md)-->替换为以下内容：

- 直接 `include`<!--[`include`](../../../ci/yaml/index.md#include)--> 语句，为父流水线提供子流水线配置。
- 放置在另一个项目中的子流水线使用 trigger API<!--[trigger API](../../../ci/triggers/)--> 而不是父子流水线功能运行。

此替代方法可确保合规性流水线不会重新启动父流水线。

### 共享和权限

对于您的仓库，您可以设置公共访问、仓库功能、文档、访问权限等功能。要从您的项目执行此操作，请转到 **设置** > **通用**，然后展开 **可见性, 项目功能, 权限** 部分。

您现在可以更改[项目可见性](../../../public_access/public_access.md)。如果您将 **项目可见性** 设置为公开，则可以将某些功能的访问权限限制为 **仅项目成员**。此外，您可以选择允许用户请求访问<!--[允许用户请求访问](../members/index.md#request-access-to-a-project)-->选项。

使用开关启用或禁用以下功能：

| 选项                          | 更多访问限制选项 | 描述   |
|:---------------------------------|:--------------------------|:--------------|
| **议题**                       | ✓                         | 激活极狐GitLab 议题跟踪器 |
| **仓库**                   | ✓                         | 启用仓库功能 |
| **合并请求**               | ✓                         | 启用合并请求功能 |
| **派生s**                        | ✓                         | 启用派生功能 |
| **Git 大文件存储 (LFS)** |                           | 允许使用大文件 |
| **软件包**                     |                           | 支持软件包库功能 |
| **CI/CD**                        | ✓                         | 启用 CI/CD 功能 |
| **Container Registry**           |                           | 为您的 Docker 镜像激活仓库 |
| **分析**                    | ✓                         | 启用分析 |
| **需求**                 | ✓                         | 控制对需求管理的访问 |
| **安全与合规**        | ✓                         | 控制对安全功能的访问 |
| **Wiki**                         | ✓                         | 启用单独的文档系统 |
| **代码片段**                     | ✓                         | 允许分享代码和文本 |
| **Pages**                        | ✓                         | 允许您发布静态网站 |
| **运维**                   | ✓                         | 控制对运维仪表盘的访问 |
| **指标仪表板**            | ✓                         | 控制对指标仪表板的访问 |

某些功能依赖于其他功能：

- 如果禁用 **议题** 选项，还会删除以下功能：
   - **议题看板**
   - [**服务台**](#服务台)

  NOTE:
  当 **议题** 选项被禁用时，您仍然可以从合并请求中访问 **里程碑**。

- 此外，如果您同时禁用 **议题** 和 **合并请求**，则无法访问：
  - **标记**
  - **里程碑**

- 如果您禁用 **仓库** 功能，还会为您的项目禁用以下功能：
  - **合并请求**
  - **CI/CD**
  - **容器镜像库**
  - **Git 大文件存储**
  - **软件包**

- 指标仪表板访问需要读取项目环境和部署。有权访问指标仪表板的用户还可以访问环境和部署。

<!--
#### Disabling the CVE ID request button **(FREE SAAS)**

> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/41203) in GitLab 13.4, only for public projects on GitLab.com.

In applicable environments, a [**Create CVE ID Request** button](../../application_security/cve_id_request.md)
is present in the issue sidebar. The button may be disabled on a per-project basis by toggling the
setting **Enable CVE ID requests in the issue sidebar**.

![CVE ID Request toggle](img/cve_id_request_toggle.png)
-->

#### 禁用电子邮件通知

项目所有者可以通过选中 **禁用电子邮件通知** 复选框来禁用与项目相关的所有[电子邮件通知](../../profile/notifications.md)。

<!--
### Merge request settings

Set up your project's merge request settings:

- Set up the merge request method (merge commit, [fast-forward merge](../merge_requests/fast_forward_merge.md)).
- Add merge request [description templates](../description_templates.md#description-templates).
- Enable [merge request approvals](../merge_requests/approvals/index.md).
- Enable [status checks](../merge_requests/status_checks.md).
- Enable [merge only if pipeline succeeds](../merge_requests/merge_when_pipeline_succeeds.md).
- Enable [merge only when all threads are resolved](../../discussions/index.md#prevent-merge-unless-all-threads-are-resolved).
- Enable [require an associated issue from Jira](../../../integration/jira/issues.md#require-associated-jira-issue-for-merge-requests-to-be-merged).
- Enable [`delete source branch after merge` option by default](../merge_requests/getting_started.md#deleting-the-source-branch).
- Configure [suggested changes commit messages](../merge_requests/reviews/suggestions.md#configure-the-commit-message-for-applied-suggestions).
- Configure [the default target project](../merge_requests/creating_merge_requests.md#set-the-default-target-project) for merge requests coming from forks.
-->

### 服务台

为您的项目启用[服务台](../service_desk.md)以提供客户支持。

<!--
### Export project

Learn how to [export a project](import_export.md#import-the-project) in GitLab.
-->

### 高级设置

在这里你可以运行管家、归档、重命名、转移、[删除一个派生关系](#删除一个派生关系)，或者删除一个项目。

#### 归档项目

归档项目使其对所有用户只读，并表明它不再被积极维护。已归档的项目也可以取消归档。只有项目所有者和管理员才有权限来归档项目。

项目归档后，仓库、包、议题、合并请求和所有其他功能都是只读的。归档项目也隐藏在项目列表中。

要归档项目：

1. 导航到您项目的 **设置 > 通用**。
1. 在 **高级** 下，单击 **扩展**。
1. 在 **归档项目** 部分，单击 **归档项目** 按钮。
1. 根据要求确认操作。

#### 取消归档项目

取消归档项目会删除项目的只读限制，并使其在项目列表中可用。只有项目所有者和管理员拥有权限来取消归档项目。

要查找存档项目：

1. 以项目所有者或具有管理员角色的用户身份登录极狐GitLab。
1. 如果您：
   - 获得项目的 URL，在浏览器中打开项目页面。
   - 没有项目的 URL：
     1. 在顶部栏上，选择 **菜单 > 项目**。
     1. 选择 **浏览项目**。
     1. 在 **排序项目** 下拉框中，选择 **显示已归档的项目**。
     1. 在 **按名称过滤** 字段中，提供项目名称。
     1. 单击项目链接以打开其 **详细信息** 页面。

接下来，要取消归档项目：

1. 导航到您项目的 **设置 > 通用**。
1. 在 **高级** 下，单击 **扩展**。
1. 在 **取消归档项目** 部分，单击 **取消归档项目** 按钮。
1. 根据要求确认操作。

#### 重命名仓库

NOTE:
只有项目维护者和管理员才有权限重命名仓库。不要与项目名称混淆，它也可以从[通用项目设置](#通用项目设置)中更改。

项目的仓库名称定义其 URL（您用于通过浏览器访问项目的 URL）及其在安装有极狐GitLab 的文件磁盘上的位置。

要重命名仓库：

1. 导航到您项目的 **设置 > 通用**。
1. 在 **高级** 下，单击 **扩展**。
1. 在 **更改路径** 下，更新仓库的路径。
1. 单击 **更改路径**。

请记住，这可能会产生意想不到的副作用，因为使用旧 URL 的每个人都无法推送或拉去。<!--阅读有关 [重命名存储库时重定向](../repository/index.md#what-happens-when-a-repository-path-changes) 会发生什么的更多信息。-->

#### 将现有项目转移到另一个命名空间

NOTE:
只有项目所有者和管理员拥有权限来转移项目。

在以下情况下，您可以将现有项目转移到[群组](../../group/index.md)中：

- 您在该群组中至少拥有**维护者**角色。
- 您至少是要转让的项目的**所有者**。
- 项目转移到的群组必须允许创建新项目。

要转移项目：

1. 导航到您项目的 **设置 > 通用**。
1. 在 **高级** 下，单击 **扩展**。
1. 在“转移项目”下，选择要将项目转移到的命名空间。
1. 按照说明输入项目路径以确认传输。

完成后，您将被重定向到新项目的命名空间。<!--在这一点上，阅读 [从旧项目重定向到新项目](../repository/index.md#what-happens-when-a-repository-path-changes) 会发生什么。-->

NOTE:
如果需要，管理员可以使用管理界面将任何项目移动到任何命名空间。

<!--
##### Transferring a GitLab.com project to a different subscription tier

When you transfer a project from a namespace that's licensed for GitLab SaaS Premium or Ultimate to Free, some data related to the paid features is deleted.

For example, [project access tokens](../../../user/project/settings/project_access_tokens.md) are revoked, and
[pipeline subscriptions](../../../ci/pipelines/multi_project_pipelines.md#trigger-a-pipeline-when-an-upstream-project-is-rebuilt)
and [test cases](../../../ci/test_cases/index.md) are deleted.
-->

#### 删除一个项目

您可以标记要删除的项目。

先决条件：

- 您必须至少具有项目的所有者角色。

要删除项目：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 展开 **高级**。
1. 在“删除项目”部分，选择 **删除项目**。
1. 根据要求确认操作。

此操作将删除包含所有关联资源（议题、合并请求等）的项目。

<!--
WARNING:
The default deletion behavior for projects was changed to [delayed project deletion](https://gitlab.com/gitlab-org/gitlab/-/issues/32935)
in GitLab 12.6, and then to [immediate deletion](https://gitlab.com/gitlab-org/gitlab/-/issues/220382) in GitLab 13.2.
-->

#### 延迟项目删除 **(PREMIUM)**

延迟一段时间后可以删除群组中的项目（不是个人命名空间）。多个设置会影响是否为特定项目启用延迟项目删除：

- 自助管理实例设置<!--[设置](../../admin_area/settings/visibility_and_access_controls.md#default-delayed-project-deletion).-->您可以启用延迟删除项目作为新组的默认设置，并配置延迟天数。<!--For GitLab.com, see the [GitLab.com settings](../../gitlab_com/index.md#delayed-project-deletion).-->
- 在群组设置中<!--[设置](../../group/index.md#enable-delayed-project-deletion)-->，为群组中的所有项目启用延迟项目删除。

#### 立即删除一个项目 **(PREMIUM)**

> 引入于 14.1 版本。

如果您不想等待，可以立即删除项目。

先决条件：

- 您必须至少具有项目的所有者角色。
- 您已[将项目标记为删除](#删除一个项目)。

要立即删除标记为删除的项目：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 展开 **高级**。
1. 在“永久删除项目”部分，选择**删除项目**。
1. 根据要求确认操作。

删除了以下内容：

- 您的项目及其仓库。
- 所有相关资源，包括议题和合并请求。

#### 恢复项目 **(PREMIUM)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/32935) in GitLab 12.6.
-->

要恢复标记为删除的项目：

1. 导航到您的项目，然后选择 **设置 > 通用 > 高级**。
1. 在恢复项目部分，点击 **恢复项目** 按钮。

#### 删除一个派生关系

派生是非项目成员为项目做出贡献<!--[为项目做出贡献](../repository/forking_workflow.md)-->的好方法。
如果您想自己使用派生，并且不需要向上游项目发送合并请求<!--[合并请求](../merge_requests/index.md)-->，您可以安全地移除派生关系。

WARNING:
一旦移除，派生关系将无法恢复。您不能向源发送合并请求，如果有人派生了您的项目，他们的派生也会失去关系。

操作步骤：

1. 导航到您项目的 **设置 > 通用 > 高级**。
1. 在 **删除派生关系** 下，单击同样标记的按钮。
1. 按照说明键入项目路径以确认操作。

NOTE:
只有项目所有者才有权限删除派生关系。

<!--
## Monitor settings

### Alerts

Configure [alert integrations](../../../operations/incident_management/integrations.md#configuration) to triage and manage critical problems in your application as [alerts](../../../operations/incident_management/alerts.md).

### Incidents

#### Alert integration

Automatically [create](../../../operations/incident_management/incidents.md#create-incidents-automatically), [notify on](../../../operations/incident_management/paging.md#email-notifications), and [resolve](../../../operations/incident_management/incidents.md#automatically-close-incidents-via-recovery-alerts) incidents based on GitLab alerts.

#### PagerDuty integration

[Create incidents in GitLab for each PagerDuty incident](../../../operations/incident_management/incidents.md#create-incidents-via-the-pagerduty-webhook).

#### Incident settings

[Manage Service Level Agreements for incidents](../../../operations/incident_management/incidents.md#service-level-agreement-countdown-timer) with an SLA countdown timer.

### Error Tracking

Configure Error Tracking to discover and view [Sentry errors within GitLab](../../../operations/error_tracking.md).

### Jaeger tracing **(ULTIMATE)**

Add the URL of a Jaeger server to allow your users to [easily access the Jaeger UI from within GitLab](../../../operations/tracing.md).

### Status Page

[Add Storage credentials](../../../operations/incident_management/status_page.md#sync-incidents-to-the-status-page)
to enable the syncing of public Issues to a [deployed status page](../../../operations/incident_management/status_page.md#create-a-status-page-project).
-->