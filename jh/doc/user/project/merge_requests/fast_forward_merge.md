---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, concepts
---

# 快进式合并请求 **(FREE)**

有时，工作流策略可能会要求没有合并提交的干净提交历史记录。在这种情况下，快进合并是完美的候选者。

使用快进合并请求，您可以保留线性 Git 历史记录以及一种无需创建合并提交即可接受合并请求的方式。

## 概览

当启用快进合并 ([`--ff-only`](https://git-scm.com/docs/git-merge#git-merge---ff-only)) 设置时，不创建合并提交并且所有合并都是快进的，这意味着只有在分支可以快进时才允许合并。

当无法进行快进合并时，用户可以选择变基。

## 启用快进式合并

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 展开 **合并请求**。
1. 在 **合并方法** 部分，选择 **快进合并**。
1. 选择 **保存更改**。

现在，当您访问合并请求页面时， **仅当可以进行快进合并时**您可以接受它。

![Fast forward merge request](img/ff_merge_mr.png)

如果无法进行快进合并但可以进行无冲突变基，则会提供变基按钮。

![Fast forward merge request](img/ff_merge_rebase.png)

如果目标分支在源分支之前并且不可能进行无冲突的变基，则需要在本地变基源分支，然后才能进行快进合并。

![Fast forward merge rebase locally](img/ff_merge_rebase_locally.png)

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
