---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 仓库存储 Rake 任务 **(FREE SELF)**

这是一组 Rake 任务，可帮助您列出现有项目及其附件，并将其迁移到 GitLab 用于组织 Git 数据的新[哈希存储](../repository_storage_types.md)。

## 列出项目和附件

以下 Rake 任务将列出 legacy 和哈希存储上可用的项目和附件。

### legacy 存储

显示摘要，然后列出使用 legacy 存储的项目及其附件：

- **Omnibus 安装实例**

  ```shell
  # Projects
  sudo gitlab-rake gitlab:storage:legacy_projects
  sudo gitlab-rake gitlab:storage:list_legacy_projects

  # Attachments
  sudo gitlab-rake gitlab:storage:legacy_attachments
  sudo gitlab-rake gitlab:storage:list_legacy_attachments
  ```

- **源安装实例**

  ```shell
  # Projects
  sudo -u git -H bundle exec rake gitlab:storage:legacy_projects RAILS_ENV=production
  sudo -u git -H bundle exec rake gitlab:storage:list_legacy_projects RAILS_ENV=production

  # Attachments
  sudo -u git -H bundle exec rake gitlab:storage:legacy_attachments RAILS_ENV=production
  sudo -u git -H bundle exec rake gitlab:storage:list_legacy_attachments RAILS_ENV=production
  ```

### 哈希存储

显示摘要，然后列出使用哈希存储项目及其附件：

- **Omnibus 安装实例**

  ```shell
  # Projects
  sudo gitlab-rake gitlab:storage:hashed_projects
  sudo gitlab-rake gitlab:storage:list_hashed_projects

  # Attachments
  sudo gitlab-rake gitlab:storage:hashed_attachments
  sudo gitlab-rake gitlab:storage:list_hashed_attachments
  ```

- **源安装实例**

  ```shell
  # Projects
  sudo -u git -H bundle exec rake gitlab:storage:hashed_projects RAILS_ENV=production
  sudo -u git -H bundle exec rake gitlab:storage:list_hashed_projects RAILS_ENV=production

  # Attachments
  sudo -u git -H bundle exec rake gitlab:storage:hashed_attachments RAILS_ENV=production
  sudo -u git -H bundle exec rake gitlab:storage:list_hashed_attachments RAILS_ENV=production
  ```

## 迁移到哈希存储

WARNING:
在 13.0 版本中，[哈希存储](../repository_storage_types.md#哈希存储) 默认启用，legacy 存储已弃用。14.0 版本消除了对 legacy 存储的支持。如果您使用的是 13.0 及更高版本，则无法将新项目切换到 legacy 存储。在管理中心中选择哈希和 legacy 存储的选项已被禁用。

此任务必须在任何配置了 Rails/Sidekiq 的机器上运行，并将安排所有现有项目和与其关联的附件迁移到 **哈希** 存储类型：

- **Omnibus 安装实例**

  ```shell
  sudo gitlab-rake gitlab:storage:migrate_to_hashed
  ```

- **源安装实例**

  ```shell
  sudo -u git -H bundle exec rake gitlab:storage:migrate_to_hashed RAILS_ENV=production
  ```

如果您有任何现有的集成，您可能需要先进行小规模部署以进行验证。您可以通过使用环境变量 `ID_FROM` 和 `ID_TO` 指定操作的 ID 范围来实现。例如，要在 Omnibus GitLab 安装中将项目 ID 限制为 50 到 100：

```shell
sudo gitlab-rake gitlab:storage:migrate_to_hashed ID_FROM=50 ID_TO=100
```

在 GitLab 中监控进度：

1. 在顶部导航栏，选择 **菜单 > 管理员**.
1. 在左侧边栏中，选择 **监控 > 后台任务**.
1. 观察 `hashed_storage:hashed_storage_project_migrate` 队列需要多长时间才能完成。达到零后，您可以通过运行上述命令来确认每个项目都已迁移。

如果您觉得有必要，您可以再次运行之前的迁移脚本来安排丢失的项目。

任何错误或警告都记录在 Sidekiq 的日志文件中。

如果启用了 Geo<!--[Geo](../geo/index.md)-->，成功迁移的每个项目都会生成一个事件来复制任何**次要**节点上的更改。

只需要 `gitlab:storage:migrate_to_hashed` Rake 任务来迁移您的仓库，但是有[附加命令](#列出项目和附件)来帮助检查 legacy 和哈希存储中的项目和附件。

## 从哈希存储回滚到 legacy 存储

WARNING:
在 13.0 版本中，[哈希存储](../repository_storage_types.md#哈希存储)默认启用，legacy 存储已弃用。14.0 版本消除了对 legacy 存储的支持。如果您使用的是 13.0 及更高版本，则无法将新项目切换到 legacy 存储。在管理中心中选择哈希和 legacy 存储的选项已被禁用。

此任务计划将所有现有项目和关联附件回滚到 legacy 存储类型。

- **Omnibus 安装实例**

  ```shell
  sudo gitlab-rake gitlab:storage:rollback_to_legacy
  ```

- **源安装实例**

  ```shell
  sudo -u git -H bundle exec rake gitlab:storage:rollback_to_legacy RAILS_ENV=production
  ```

如果您有任何现有的集成，您可能需要先做一个小的回滚，以进行验证。您可以通过使用环境变量 `ID_FROM` 和 `ID_TO` 指定操作的 ID 范围来实现。例如，要在 Omnibus GitLab 安装中将项目 ID 限制为 50 到 100：

```shell
sudo gitlab-rake gitlab:storage:rollback_to_legacy ID_FROM=50 ID_TO=100
```

您可以在**管理中心 > 监控 > 后台作业** 页面监控进度。在 **Queues** 选项卡上，您可以查看 `hashed_storage:hashed_storage_project_rollback` 队列以了解该过程需要多长时间才能完成。

达到零后，您可以通过运行上述命令来确认每个项目都已回滚。如果某些项目没有回滚，您可以再次运行此回滚脚本以安排进一步的回滚。任何错误或警告都记录在 Sidekiq 的日志文件中。

如果您有 Geo 设置，回滚将不会自动反映在**次要**节点上。您可能需要等待回填操作启动并手动从特殊的 `@hashed/` 文件夹中删除剩余的仓库。
