---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# GitHub 导入 **(FREE SELF)**

<!--
> [Introduced]( https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/10308) in GitLab 9.1.
-->

要检索和导入 GitHub 仓库，您需要一个 [GitHub 个人访问令牌](https://github.com/settings/tokens)。用户名应该作为第二个参数传递给 Rake 任务，它成为项目的所有者。您可以使用相同的命令恢复导入。

请记住，语法非常具体。删除参数块内和括号之前/之后的任何空格。此外，一些 shell（例如，`zsh`）可以单独解释开/关括号（`[]`）。您可能需要转义括号或使用双引号。

## 注意事项

如果在导入时达到 GitHub [速率限制](https://docs.github.com/en/rest/reference/rate-limit)，则导入过程将等待 (`sleep()`)，直到可以继续导入。

## 导入多个项目

要从可用的 GitHub 项目列表中导入项目：

```shell
# Omnibus installations
sudo gitlab-rake "import:github[access_token,root,foo/bar]"

# Installations from source
bundle exec rake "import:github[access_token,root,foo/bar]" RAILS_ENV=production
```

在这种情况下，`access_token` 是 GitHub 个人访问令牌，`root` 是 GitLab 用户名，`foo/bar` 是从您的 GitHub 项目创建的新 GitLab 命名空间/项目。子组也是可能的：`foo/foo/bar`。

## 导入单个项目

要导入特定的 GitHub 项目（此处名为 `foo/github_repo`）：

```shell
# Omnibus installations
sudo gitlab-rake "import:github[access_token,root,foo/bar,foo/github_repo]"

# Installations from source
bundle exec rake "import:github[access_token,root,foo/bar,foo/github_repo]" RAILS_ENV=production
```
