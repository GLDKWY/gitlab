---
stage: Manage
group: Import
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 翻译

我们使用 [Crowdin](https://crowdin.com) 管理翻译流程。
要在 [`translate.gitlab.com`](https://translate.gitlab.com) 上贡献翻译，您必须创建一个 Crowdin 帐户。您可以创建一个新帐户或使用支持的任何登录服务。

## 语言选择

<!--
GitLab is being translated into many languages. To select a language to contribute to:
-->

<!--
   - If the language you want is available, proceed to the next step.
   - If the language you want is not available,
      [open an issue](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=✓&state=all&label_name[]=Category%3AInternationalization).
      Notify our Crowdin administrators by including `@gitlab-org/manage/import` in your issue.
   - After the issue and any merge requests are complete, restart this procedure.
-->

1. 在 [GitLab Crowdin 项目](https://crowdin.com/project/gitlab-ee) 中找到您想要贡献的语言。

1. 查看文件和文件夹列表。选择 `master/locale/GitLab` 打开翻译编辑器。

### 翻译编辑器

在线翻译编辑器是贡献翻译的最简单方法。

![Crowdin Editor](img/crowdin-editor.png)

- 用于翻译的字符串列在左侧面板中。
- 翻译被输入到中央面板中。<!--包含复数的字符串需要多次翻译。-->要翻译的字符串显示在上图中，其中突出显示了词汇表术语。如果要翻译的字符串不清楚，您可以请求上下文。

右侧面板的 **Terms** 选项卡中提供了常用术语词汇表。在 **Comments** 选项卡中，您可以添加评论，与社区讨论翻译。

请记住**保存**每个翻译。

<!--
## 一般翻译指南

在翻译任何字符串之前，请务必确认以下准则。
-->

### 带有命名空间的字符串

命名空间位于字符串之前，并由 `|`（`namespace|string`）分隔。当您在源字符串之前看到命名空间时，您应该从最终翻译中删除命名空间。例如，在 `OpenedNDaysAgo|Opened` 中，移除 `OpenedNDaysAgo|`。<!--If translating to
French, translate `OpenedNDaysAgo|Opened` to `Ouvert•e`, not `OpenedNDaysAgo|Ouvert•e`.-->

<!--
### 技术术语

您应该将一些技术术语视为专有名词，不需要翻译它们。使用 [`translate.gitlab.com`](https://translate.gitlab.com) 时，应在词汇表中注明应始终使用英语的技术术语。
You should treat some technical terms like proper nouns and not translate them. Technical terms that
should always be in English are noted in the glossary when using
[`translate.gitlab.com`](https://translate.gitlab.com).
This helps maintain a logical connection and consistency between tools (for example, a Git client)
and GitLab.

To find the list of technical terms:

1. Go to [`translate.gitlab.com`](https://translate.gitlab.com).
1. Select the language to translate.
1. Select **Glossary**.

### Formality

The level of formality used in software varies by language:

| Language | Formality | Example |
| -------- | --------- | ------- |
| French   | formal    | `vous` for `you` |
| German   | informal  | `du` for `you` |

Refer to other translated strings and notes in the glossary to assist you in determining a suitable
level of formality.

### Inclusive language

[Diversity, inclusion, and belonging](https://about.gitlab.com/handbook/values/#diversity-inclusion)
are GitLab values. We ask you to avoid translations that exclude people based on their gender or
ethnicity. In languages that distinguish between a male and female form, use both or choose a
neutral formulation.
-->

<!-- vale gitlab.Spelling = NO -->

<!--
For example, in German, the word _user_ can be translated into _Benutzer_ (male) or _Benutzerin_
(female). Therefore, _create a new user_ translates to _Benutzer(in) anlegen_.
-->

<!-- vale gitlab.Spelling = YES -->

<!--
### Updating the glossary

To propose additions to the glossary, please
[open an issue](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=✓&state=all&label_name[]=Category%3AInternationalization).

## French translation guidelines
-->

<!-- vale gitlab.Spelling = NO -->

<!--
In French, the _écriture inclusive_ is now over (see on [Legifrance](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000036068906/)).
To include both genders, write _Utilisateurs et utilisatrices_ instead of _Utilisateur·rice·s_. If
there is not enough space, use the male gender alone.
-->

<!-- vale gitlab.Spelling = YES -->
